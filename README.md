# bot-template

Um simples bot para Discord ;=;

# Instalações

As instalações necessárias para o bot funcionar são :
```
discord.js
fs
```
Elas são as principais, as outras serão citadas nos comandos para alguma coisa como **superagent**

# Template

Para qualquer criação de comandos você precisa seguir esse padrão :
```js
module.exports = {
	config: {
		name: "nome do comando",
		desc: "descrição do comando",
		aliases: ["primeira aliases", "segunda aliases", "terceira", "quarta"],
		use: "Como usa o comando"
	},
	run: async (client, message, args, Discord) => {
	//comando
	}
}
```
Não precisa ser na ordem igualzinha ao que está ali e não precisa-se ter quatro aliases dentro do comando

# Créditos

Aviso que todos os créditos desse projeto de mini-bot são todos para Yukkie ( 《$uni👒》#2361 ) no caso, euzinha :3, qualquer tentativa de cópia sem créditos não será tolerada :v

# Tutorial

Iniciante em criação de bots ? hummm, recomendo ler o Tutorial.md ;w; ( conselho de amiga )

# Fim

É isso, espero que consigam melhorar seus bots com esse template e que cada dia mais pessoas evoluam e aumentem seu conhecimento na criação de Bots e na programação em si :v