/*
**** Constantes
*/

const Discord = require('discord.js') //Aqui definimos a discord.js em nosso código, caso dê erro nessa linha, só dê npm i discord.js no console ;v
const { bot, client, Cliente } = new Discord.Client(); // Aqui definimos o client ( bot )
const fs = require('fs') //Aqui definimos o fs em nosso código, ele vai servir pro nosso Handler, caso dê erro nessa linha, só dê npm i fs no console ;v
const config = require('./utils/config.js') //Aqui definimos o config.js em nosso código, ele vai servir pro bot logar e sabermos o prefix do bot sem definir algo novamente, caso dê erro nessa linha, cheque o arquivo config.js, digitação dessa linha e se o arquivo config.js e a pasta utils existem :v
const aliases = new Discord.Collection(); //essa será a collection responsável pelo armazenamento das aliases
const comandos = new Discord.Collection(); //essa será a collection que irá guardar o nome/local dos comandos
/*
**** Handler
*/

fs.readdir('./comandos', (error, file) => { //pedimos para o fs ler a pasta comandos
    if(error) return console.log(error); //se tiver um erro, ele não executará o que estiver abaixo e irá retornar com um log no console
    const files = file.filter(f=>f.split('.').pop() === 'js') //aqui a gente define files para diferenciar os arquivos com .js dos sem .js
    files.forEach((f,i) => { // aqui dizemos que para cada arquivo com .js na pasta, iremos fazer a seguinte coisa :
        const comando = require(`./comandos/${f}`) //iremos definir comando
        comandos.set(comando.config.name, comando) //iremos setar o nome do comando/local na collection comandos
        comando.config.aliases.forEach(alias=>{ //aqui dizemos que para cada aliases do comand
            aliases.set(alias, comando.config.name) //setar a aliases/nome do comando na collection aliases
        })
    })
})

/*
**** Primeiro Evento
*/

bot.on("ready", async () => { // esse será o evento de quando o bot logar no token :v
    const nome = client.user.username;
    const id = client.user.id;
    const servers = client.guilds.size;
    const users = client.users.size;
    console.log('=================')
    console.log(`Nome [${nome}]`)
    console.log(`ID [${id}]`)
    console.log(`Servidores que está [${servers}]`)
    console.log(`Usuários(as) [${users}]`)
    console.log('=================')
    client.user.setActivity(config.config.prefix + 'help', {type: 'PLAYING'});
    /*
    * PLAYING = Jogando. Exemplo - client.user.setActivity('oi', {type: 'PLAYING'});
    * WATCHING = Assistindo. Exemplo - client.user.setActivity('oi', {type: 'WATCHING'});
    * LISTENING = Ouvindo. Exemplo - client.user.setActivity('oi', {type: 'LISTENING'});
    * STREAMING = Transmitindo. Exemplo - client.user.setActivity('oi', {type: 'STREAMING', url: 'twitch'});
    */
})

/*
**** Evento Message
*/

bot.on("message", async function(message){ // Sempre que ocorrer uma mensagem, esse evento é executado ;v
    if(message.author.bot) return; // Se o author da mensagem, ou seja, quem mandou a mensagem, for um bot, ele não executará o código
    if(message.channel.type == 'dm') return; // Se o canal que a mensagem foi mandada for na DM ( Direct Messages, ou seja, as mensagens privadas ), ele não executará o código
    if(!message.content.startsWith(config.config.prefix)) return; // Se a mensagem não começar com o prefix, ele não executará o código
    
    let args = message.content.slice(config.config.prefix.length).trim().split(/ +/g); // Aqui definimos args
    let comando = args.shift().toLowerCase(); // Definição de comando
    
    let cmd = comandos.get(comando) || aliases.get(comandos.get(comando)); // Aqui definimos "cmd" para buscar os comandos na collection
    if(cmd){ // se existir o comando
        cmd.run(client, message,args,Discord); // executará o comando usando a function "run"
    }
})

//Login

bot.login(config.config.token);